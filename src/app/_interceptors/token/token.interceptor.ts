import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { SIPNAT_ACCESS_TOKEN } from "app/_utils/constants";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        if (
            request.url.includes("signin") ||
            request.url.includes("signup") ||
            request.url.includes("refresh")
        ) {
            return next.handle(request);
        } else {
            let token = localStorage.getItem(SIPNAT_ACCESS_TOKEN);
            if (token != null) {
                const authReq = request.clone({
                    headers: request.headers.set(
                        "Authorization",
                        "Bearer " + token
                    ),
                });
                return next.handle(authReq);
            }
        }
        return next.handle(request);
    }
}
