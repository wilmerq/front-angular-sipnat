import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";

@Injectable({
    providedIn: "root",
})
export class DataService {
    private endpoint: string;

    constructor(private http: HttpClient) {
        this.endpoint = `${environment.host}/data`;
    }

    listDataToday(idSensor: number) {
        return this.http.get<any>(`${this.endpoint}/${idSensor}`);
    }

    listAllData(idSensor: number) {
        return this.http.get<any>(`${this.endpoint}/${idSensor}/all`);
    }
}
