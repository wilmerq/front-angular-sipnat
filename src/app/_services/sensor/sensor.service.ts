import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Sensor } from "app/_model/Sensor";
import { environment } from "environments/environment";

@Injectable({
    providedIn: "root",
})
export class SensorService {
    private endpoint: string;

    constructor(private http: HttpClient) {
        this.endpoint = `${environment.host}/sensor`;
    }

    create(sensor: Sensor) {
        return this.http.post<any>(`${this.endpoint}`, sensor);
    }

    list() {
        return this.http.get<Sensor[]>(`${this.endpoint}`);
    }
}
