import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { User } from 'app/_model/User';
import { AuthenticationService } from 'app/_services/authentication/authentication.service';
import swal from "sweetalert2";
import { Router } from '@angular/router';
@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy {
    registerForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            name: ['', Validators.required],
            lastName: ['', Validators.required],
            phoneNumber: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        this.registerForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    get controls() {
        return this.registerForm.controls;
    }

    onSubmit() {
        if (this.registerForm.valid) {
            let user: User = new User();
            user.firstname = this.controls.name.value;
            user.lastname = this.controls.lastName.value;
            user.phoneNumber = this.controls.phoneNumber.value;
            user.email = this.controls.email.value;
            user.password = this.controls.password.value;

            this.authenticationService.signup(user).subscribe(resp => {
                swal.fire({
                    title: 'Registro con éxito',
                    showDenyButton: false,
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                    icon: 'success'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.router.navigate(['/pages/auth/login']);
                    }
                })

            }, error => {
                console.log(error);

                if (error.error != null && error.error != undefined) {
                    swal.fire('Error en el registro', `Se ha presentado un error al momento de registrarse, el sistema ha dado como respuesta ${error.error.mensaje}`, 'error');
                    return;
                }
                swal.fire('Error en el registro', `Se ha presentado un error al momento de registrarse, por favor intente nuevamente`, 'error');
            });
        }

    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};
