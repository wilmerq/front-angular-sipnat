import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalGraphDataSensorComponent } from './modal-graph-data-sensor.component';

describe('ModalGraphDataSensorComponent', () => {
  let component: ModalGraphDataSensorComponent;
  let fixture: ComponentFixture<ModalGraphDataSensorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalGraphDataSensorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGraphDataSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
