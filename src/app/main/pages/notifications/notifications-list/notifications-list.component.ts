import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Notification } from 'app/_model/Notification';
import { NotificationService } from 'app/_services/notification/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'app-notifications-list',
  templateUrl: './notifications-list.component.html',
  styleUrls: ['./notifications-list.component.scss'],
  animations: fuseAnimations
})
export class NotificationsListComponent implements OnInit {

  currentCategory: string;
  categories: any[] = ["Activas", "Revisadas"];
  filteredCourses: Notification[];
  notifications: Notification[];

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
    moment.locale('es');

    this.notificationService.list().subscribe(resp => {
      this.filteredCourses = resp;
      this.notifications = resp;
    });

  }

  formatDateFromNow(date) {
    return moment(date).fromNow()
  }

  formatDate(date) {
    return moment(date).format('lll');
  }

  filterCoursesByCategory(): void {
    // Filter
    if (this.currentCategory === 'all') {
      this.filteredCourses = this.notifications;
    } else {
      this.filteredCourses = this.notifications.filter((course) => {
        let typeTemp: string;
        if (this.currentCategory == "Activas") {
          typeTemp = "ROJA";
        } else if (this.currentCategory == "Revisadas") {
          typeTemp = "GRIS";
        }

        return course.type === typeTemp;
      });
    }
  }

}
