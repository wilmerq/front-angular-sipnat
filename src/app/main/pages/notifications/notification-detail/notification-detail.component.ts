import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { Notification } from 'app/_model/Notification';
import { NotificationService } from 'app/_services/notification/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.scss'],
  animations: fuseAnimations
})
export class NotificationDetailComponent implements OnInit {

  id: any;
  notification: Notification;
  form: FormGroup;
  categories: any[] = ["Activa", "Revisada"];
  currentType;


  constructor(private _route: ActivatedRoute, private notificationService: NotificationService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    moment.locale('es');
    this.inicializateFormGroup();

    this.id = this._route.snapshot.paramMap.get('id');
    if (this.id != null && this.id != undefined) {
      this.notificationService.getById(this.id).subscribe(resp => {
        this.notification = resp;
        if (resp.type == "ROJA") {
          this.form.get("newStatus").setValue("Activa");
          this.currentType = "Activa";
        } else if (resp.type == "GRIS") {
          this.form.get("newStatus").setValue("Revisada");
          this.currentType = "Revisada";
        }
      })
    }


  }

  formatDate(date) {
    return moment(date).format('LLL');
  }

  inicializateFormGroup() {
    this.form = this.formBuilder.group({
      newStatus: ['']
    });
  }

  updateStatus() {
    if (this.form.valid) {
      let status = this.form.get("newStatus").value;

      if (status == "Activa") {
        this.notification.type = "ROJA"
      } else {
        this.notification.type = "GRIS"
      }

      this.notificationService.updateNotication(this.notification).subscribe(resp => {
        this.router.navigate(['/pages/notifications'])
      })
    }
  }
}
