FROM node:12.7-alpine AS buildAngular
WORKDIR /app/
COPY ./ /app/
RUN npm ci
RUN npm run build -- --prod
RUN mv /app/dist/fuse/* /app/dist/


FROM nginx:1.13.8-alpine 
COPY --from=buildAngular /app/dist/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf